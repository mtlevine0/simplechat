// set up ======================================================================
var express  	= require('express');										// express for http
var app     		= express(); 													// create our app w/ express
var mongoose 	= require('mongoose'); 									// mongoose for mongodb
var port  	 		= process.env.PORT || 8080; 							// set the port
var database 	= require('./config/database'); 						// load the database config

// configuration ==================================================================
mongoose.connect(database.url); 											// connect to mongoDB database on modulus.io

app.configure(function() {
	app.use(express.static(__dirname + '/public')); 					// set the static files location /public/img will be /img for users
	app.use(express.logger('dev')); 											// log every request to the console
	app.use(express.bodyParser()); 											// pull information from html in POST
	app.use(express.methodOverride()); 									// simulate DELETE and PUT
	app.use(express.cookieParser());										// Enable cookies
	app.use(express.session({secret: '1234567890QWZRTY'}));	// Enable sessions. TODO: generate secure secrete token
});

// routes =======================================================================
require('./app/routes.js')(app);

// listen (start app with node server.js) ====================================================
app.listen(port);
console.log("App listening on port " + port);