var scotchTodo = angular.module('SimpleChat', []);
var loggedIn = false;

function mainController($scope, $http) {
	$scope.messageForm = {};

	// Set up the initial look of the page
	if(!loggedIn){
		$('#userDisplay').hide();
		$('#logout-btn').hide();
		$('#main').block({ 
			message: null
		});
	}
	
	// Fetch messages from the message database every 500ms
	setInterval(function(){
		console.log('called');
		$http.get('/api/messages')
			.success(function(data) {
				$scope.messages = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	},500);
	
	// On initial page load display all messages
	$http.get('/api/messages')
		.success(function(data) {
			$scope.messages = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
	});
	
	// On page load get the user signed in
	$http.get('/api/users')
		.success(function(data) {
			$scope.users = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data); 
	});
	
	// Send a message to the mongodb message database
	$scope.sendMessage = function() {
		if(loggedIn == true){
			if($scope.messageForm.text != null && $scope.messageForm.text != ''){
				$scope.messageForm.text = $scope.users + ': ' + $scope.messageForm.text;
				$http.post('/api/messages', $scope.messageForm)
					.success(function(data) {
						$scope.messageForm = {};
						$scope.messages = data;
						console.log(data);
					})
					.error(function(data) {
						console.log('Error: ' + data);
					});
			}
		}else{
			alert("please login");
		}
	};

	// Clear all the messages from from the chat database
	$scope.clearMessages = function() {
		$http.delete('/api/messages/')
			.success(function(data) {
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};
	
	// Log the user in to the chat
	$scope.login = function() {
		if($scope.loginForm.text != null && $scope.loginForm.text != ''){
			$('#main').unblock();
			$('#logginForm').fadeOut("fast");
			$('#userDisplay').fadeIn("fast");
			$('#logout-btn').fadeIn("fast");
			loggedIn = true;
			$http.post('/api/users/login', $scope.loginForm)
				.success(function(data) {
					$scope.users = data;
				})
				.error(function(data) {
					console.log('Error: ' +data);
				});
		}
	};
		
	// Log the user out of the chat
	$scope.logout = function() {
		$('#main').block({ 
			message: null
		});
		$('#userDisplay').fadeOut("fast");
		$('#logginForm').fadeIn("fast");
		$('#logout-btn').fadeOut("fast");
		$scope.loginForm.text = null;
		loggedOut = false;
		$http.get('/api/logout/')
			.success(function(data) {
				$scope.users = null;
			})
			.error(function(data) {
				console.log('Error: ' + data);
		});	
	};

}
