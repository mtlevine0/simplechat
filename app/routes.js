var Message = require('./models/message');

module.exports = function(app) {

	// api ---------------------------------------------------------------------
	// Get all of the messages in the database
	app.get('/api/messages', function(req, res) {
		Message.find(function(err, messages) {
			if (err)
				res.send(err)
			res.json(messages); // return all todos in JSON format
		});
	});

	// Create a message entry in the database and return the entire database 
	app.post('/api/messages', function(req, res) {
		Message.create({
			text : req.body.text,
			user : req.body.user
		}, function(err, messages) {
			if (err)
				res.send(err);
			Message.find(function(err, messages) {
				if (err)
					res.send(err)
				res.json(messages);
			});
		});

	});

	// Clear the entire message database
	app.delete('/api/messages/', function(req, res) {
		Message.remove(function(err, message) {
			if (err)
				res.send(err);
			Message.find(function(err, messages) {
				if (err)
					res.send(err)
				res.json(messages);
			});
		});
	});
	
	app.post('/api/users/login', function(req, res) {
		req.session.name = req.body.text
		console.log(req.session.name);
		res.send(req.session.name);
	});

	app.get('/api/logout', function(req, res) {
		delete req.session.name;
	});
	
	app.get('/api/users', function(req, res) {
		res.send(req.session.name);
	});
	
	// application -------------------------------------------------------------
	app.get('*', function(req, res) {
		res.sendfile('./public/main.html');
	});
};
