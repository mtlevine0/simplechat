# SimpleChat

SimpleChat is a MEAN based chat room app that runs in the browser.

Node provides the RESTful API. Angular provides the frontend and accesses the API. MongoDB stores like a hoarder.

## Requirements

- [Node and npm](http://nodejs.org)

## Installation

1. Clone the repository: `git clone git@bitbucket.org:mtlevine0/simplechat.git`
2. Install the application: `npm install`
3. Start the server: `node server.js`
4. View in browser at `http://localhost:8080`

## Sources

SimpleChat is based off of the Todo-app tutorial which can be found on scotch.io

http://scotch.io/tutorials/javascript/creating-a-single-page-todo-app-with-node-and-angular